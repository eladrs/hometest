<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('/{id}/{name}', function ($id,$name) {
    return "my name is"." ". $name. "and my id is:"." ". $id;
});
*/

Route::resource('tasks','TasksController')->middleware('auth');
Route::get('tasks/done/{id}','TasksController@done')->name('done');
Route::get('tasks/wrote/{user_id}','TasksController@wrote')->name('wrote');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
