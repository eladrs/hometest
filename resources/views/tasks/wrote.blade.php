@extends('layouts.app')
@section('content')
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> 

    <div class='container' >
     <button type="button" class="btn btn-dark btn-lg btn-block">
      <a href="{{route('tasks.index')}}"> show all tasks</a></button>
    </div>

</div>
<div class='container'>
<br><br>
  <table class="table table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col">id</th>
      <th scope="col"> task title</th>
      <th scope="col"> delete task</th>
      <th scope="col">created at</th>
      <th scope="col">updated at</th>
      <th scope="col">status</th>
    </tr>
  </thead>
  <tbody>
  @foreach($tasks as $task)
  @if(auth()->user()->id == $task->user_id)
    <tr>
      <td>{{$task->id}}</td>
      <td>{{$task->title}}<a href = "{{route('tasks.edit',$task->id)}}" > edit</a></td>
      <td>@cannot('user')<form method="post" action ="{{action('TasksController@destroy',$task->id)}}">
      @csrf
      @method('DELETE')
      <input type ="submit" class="form-control" name="submit" value ="delete">
      </form> @endcannot</td>
      <td>{{$task->created_at}}</td> 
      <td>{{$task->updated_at}}</td>
      <td>
        @if($task->status==0)
        @cannot('user') 
          <a href="{{route('done',$task->id)}}">@endcannot mark as done</a>
        @else
        Done  
        @endif  
     </td> 
     </tr>
   @else
   @endif  
  @endforeach
  </tbody>
  </table>
  </div>
@endsection