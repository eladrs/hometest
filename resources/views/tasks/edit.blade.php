@extends('layouts.app')
@section('content')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


    <div class ="container">
      <div class="col-2 offset-10">
      <a href = "{{route('tasks.index',$task->id)}}" > back to tasks list</a>
      </div>
    </div>
    <!--validation errors-->
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<!--update-->
<div class="container">
  <h3>Update task</h3>
    <form method="post" action ="{{action('TasksController@update',$task->id)}}">
     @csrf
     @method('PATCH')
        <div class="form-group">
                <label for="title">task to update</label>
                <input type ="text" class ="form-control" name="title" value = "{{$task->title}}">
        </div>
        <div class="form-group">
                <label for = "date" >Updated at </label>
                <input type="date" class = "form-control" name="updated_at" value = "{{$task->updated_at}}">
        </div>  
        <div class ="container">
            <div class="col-4  offset-4">
                <input type ="submit" class="form-control btn btn-secondary" name="submit" value ="Save Update"> 
            </div>
        </div>
    </form>
</div><br>
<!--Delete
<form method="post" action ="{{action('TasksController@destroy',$task->id)}}">
@csrf
@method('DELETE')
   <div class ="container">
    <div class ="container">
        <div class="col-4  offset-4">
             <input type ="submit" class="form-control btn btn-secondary" name="submit" value ="Delete"> 
        </div>
    </div>
   </div>
</form>-->
@endsection

