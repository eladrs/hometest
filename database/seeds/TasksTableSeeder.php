<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([
            [
                'id' => '1',
                'user_id' => '1',
                'title' => 'test task 1',
                'status' => 0,
                'created_at' => date('Y-m-d G:i:s'),
            ],
            [
                'id' => '2',
                'user_id' => '1',
                'title' => 'test task 2',
                'status' => 0,
                'created_at' => date('Y-m-d G:i:s'),
            ],
            [
                'id' => '3',
                'user_id' => '1',
                'title' => 'test task 3',
                'status' => 0,
                'created_at' => date('Y-m-d G:i:s'),
            ]
        ]);
    }
}
