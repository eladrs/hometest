<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;


class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $id = Auth::id();
       $tasks = Task::all();
       return view('tasks.index',compact('tasks'));
    }
    public function wrote($user_id)
    {
       $tasks = Task::all();
       return view('tasks.wrote',compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required'
        ]);
        $task=new Task();
        $id = Auth::id();
        $task->title=$request->title;
        $task->user_id=$id;
        $task->status=0;
        $task->save();
        return redirect('tasks');
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        return view('tasks.edit',compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task=Task::findOrFail($id);
        if(!$task->user->id ==Auth::id()) return (redirect('tasks'));
        $this->validate($request,[
           'title'=>'required',
          'updated_at'=>'required|date|before_or_equal:today']);
        
        /*if (Gate::denies('manager')) {
            if ($request->has('title'))
                   abort(403,"You are not allowed to edit tasks..");
        }
          $task->update($request->except(['_token']));
        if ($request->ajax()){
            return Response::json(array('result'=>'success', 'status'=>$request->status),200);
        }*/
        return redirect('tasks');
    }   
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::denies('admin'))
        {
            abort(403,"Sorry you are not allowed to delete tasks..");
        }
        $task = Task::find($id);
        if(!$task->user->id ==Auth::id()) return (redirect('tasks'));
        $task ->delete();
        return redirect('tasks');
    }
    public function done($id)
    {
        if(Gate::denies('admin'))
        {
            abort(403,"Sorry you are not allowed to mark as done..");
        }
        $task=Task::findOrFail($id);
        $task->status=1;
        $task->save();
        return redirect('tasks');
    }

}
